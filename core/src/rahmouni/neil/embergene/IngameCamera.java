package rahmouni.neil.embergene;

import com.badlogic.gdx.math.Vector3;

public class IngameCamera {
    private static Vector3 position = new Vector3(0, 0, 0);
    private static Vector3 velocity = new Vector3(0, 0, 0);
    private static Vector3 size = new Vector3(Embergene.WIDTH, Embergene.HEIGHT, 0);

    public static void update(float dt, Vector3 targetPos, int farX, int farY) {
        int minWidth = 2 * Math.abs((int) targetPos.x - farX) + 200;
        int minHeight = 2 * Math.abs((int) targetPos.y - farY) + 400;

        if (minWidth * Embergene.HEIGHT / Embergene.WIDTH >= minHeight) {
            size.set(minWidth, (float) minWidth * Embergene.HEIGHT / Embergene.WIDTH, 0);
        } else {
            size.set((float) minHeight * Embergene.WIDTH / Embergene.HEIGHT, minHeight, 0);
        }

        targetPos.sub(position);
        targetPos.scl(2f);
        velocity.add(targetPos);
        velocity.scl(dt);
        position.add(velocity);
        velocity.scl(.5f / dt);
    }

    public static Vector3 getPosition() {
        return position;
    }

    public static void setPosition(Vector3 pos) {
        position = pos;
        velocity = new Vector3(0, 0, 0);
    }

    public static Vector3 getSize() {
        return size;
    }
}
