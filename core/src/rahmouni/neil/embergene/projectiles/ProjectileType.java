package rahmouni.neil.embergene.projectiles;

public enum ProjectileType {
    BULLET("images/projectiles/bullet.png", 30, 1000);

    private String texturePath;
    private int damage;
    private int speed;

    ProjectileType(String texturePath, int damage, int speed) {
        this.texturePath = texturePath;
        this.damage = damage;
        this.speed = speed;
    }

    public String getTexturePath() {
        return texturePath;
    }

    public int getDamage() {
        return damage;
    }

    public int getSpeed() {
        return speed;
    }
}
