package rahmouni.neil.embergene.projectiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import rahmouni.neil.embergene.Embergene;
import rahmouni.neil.embergene.states.types.InGameState;

public class Projectile {
    private ProjectileType type;
    private Vector2 position;
    private Vector2 direction;
    private Rectangle hurtBox;

    private Texture texture;

    public Projectile(ProjectileType type, Vector2 position, Vector2 direction) {
        this.type = type;
        this.position = position;
        this.direction = direction;
        texture = new Texture(type.getTexturePath());
        hurtBox = new Rectangle(this.position.x, this.position.y, texture.getWidth(), texture.getHeight());

        InGameState.projectiles.add(this);
    }

    public void dispose() {
        InGameState.projectiles.remove(this);
        texture.dispose();
    }

    public void render(SpriteBatch sb) {
        sb.draw(texture, position.x, position.y);
    }

    public void update(float dt) {
        direction.scl(dt * type.getSpeed());
        position.add(direction);
        direction.scl(1 / (dt * type.getSpeed()));

        if (position.x < -Embergene.WIDTH
                || position.x > Embergene.WIDTH * 2
                || position.y < -Embergene.HEIGHT
                || position.y > Embergene.HEIGHT * 2) {

            dispose();
        }
    }

    public Rectangle getHurtBox() {
        return hurtBox;
    }
}
