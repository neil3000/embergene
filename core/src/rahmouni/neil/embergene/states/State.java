package rahmouni.neil.embergene.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import rahmouni.neil.embergene.Embergene;
import rahmouni.neil.embergene.network.Connection;

public abstract class State {
    protected static Connection connection;
    protected Embergene embergene;
    protected OrthographicCamera cam;
    protected OrthographicCamera hudCam;
    private StateType type;

    protected State(StateType type) {
        this.type = type;

        cam = new OrthographicCamera(Embergene.WIDTH, Embergene.HEIGHT * ((float) Gdx.graphics.getHeight() / Gdx.graphics.getWidth()));
        cam.position.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f, 0);
        cam.update();
        hudCam = new OrthographicCamera(Embergene.WIDTH, Embergene.HEIGHT * ((float) Gdx.graphics.getHeight() / Gdx.graphics.getWidth()));
        hudCam.position.set(hudCam.viewportWidth / 2f, hudCam.viewportHeight / 2f, 0);
        hudCam.update();
    }

    public abstract void update(float dt);

    public abstract void render(SpriteBatch sb);

    public abstract void dispose();

    public abstract void resize(int width, int height);

    public void setEmbergene(Embergene embergene) {
        this.embergene = embergene;
    }

    public StateType getType() {
        return type;
    }
}