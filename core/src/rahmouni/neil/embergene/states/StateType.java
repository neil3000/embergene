package rahmouni.neil.embergene.states;

public enum StateType {
    TITLE_SCREEN("On the title screen"),
    IN_LOBBY("Waiting in a lobby"),
    IN_GAME("In a game");

    private String displayName;

    StateType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
