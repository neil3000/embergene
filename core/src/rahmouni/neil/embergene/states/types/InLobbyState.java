package rahmouni.neil.embergene.states.types;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rahmouni.neil.embergene.Player;
import rahmouni.neil.embergene.animations.Animation;
import rahmouni.neil.embergene.animations.AnimationType;
import rahmouni.neil.embergene.champions.ChampionType;
import rahmouni.neil.embergene.hud.DamageBar;
import rahmouni.neil.embergene.network.events.JoinedRoomEvent;
import rahmouni.neil.embergene.states.State;
import rahmouni.neil.embergene.states.StateType;

public class InLobbyState extends State {
    private List<Player> players = new ArrayList<>();
    private List<DamageBar> damageBars = new ArrayList<>();
    private String roomID;
    private long startTimestamp;

    private Viewport viewport;

    InLobbyState() {
        super(StateType.IN_LOBBY);

        viewport = new ScreenViewport(cam);

        connection.sendRoomSearchingPacket();
        connection.addListener(new JoinedRoomEvent() {
            @Override
            public void call(String eventRoomID, List<Player> eventPlayers) {
                players = eventPlayers;
                roomID = eventRoomID;

                for (int i = 0; i < eventPlayers.size(); i++) {
                    damageBars.add(new DamageBar(ChampionType.MIKHAIL, 0, i));
                }

                Gdx.app.log("RahNeil_N3:networkStatus", "joined room " + roomID + " with players:\n" + new JSONObject(players).toString());
            }
        });

        new Animation(AnimationType.MIKHAIL_PREVIEW);

        startTimestamp = TimeUtils.millis();
    }

    @Override
    public void update(float dt) {
        for (DamageBar b : damageBars) b.update(dt);
        Gdx.app.log("test", damageBars.size() + "");
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.begin();

        sb.draw(new Animation(AnimationType.MIKHAIL_PREVIEW).getFrame(), 0, 0);
        for (DamageBar b : damageBars) b.render(sb);

        sb.end();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void dispose() {
        for (DamageBar b : damageBars) b.dispose();
    }

    public long getStartTimestamp() {
        return startTimestamp;
    }
}
