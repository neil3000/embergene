package rahmouni.neil.embergene.states.types;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import rahmouni.neil.embergene.Embergene;
import rahmouni.neil.embergene.network.Connection;
import rahmouni.neil.embergene.network.events.ConnectionEstablishedEvent;
import rahmouni.neil.embergene.states.State;
import rahmouni.neil.embergene.states.StateType;

public class TitleScreenState extends State {
    private Texture button;
    private Viewport viewport;

    public TitleScreenState() {
        super(StateType.TITLE_SCREEN);

        button = new Texture("images/button.png");
        viewport = new ScreenViewport(cam);
        if (connection == null) {
            final Connection c = new Connection();
            c.addListener(new ConnectionEstablishedEvent() {
                @Override
                public void call() {
                    Gdx.app.log("RahNeil_N3:Connection", "connection established");
                    connection = c;
                }
            });
        }
    }

    @Override
    public void update(float dt) {
        if (Gdx.input.justTouched()) {
            if (connection != null) embergene.setState(new InLobbyState());
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.begin();
        sb.draw(button, Embergene.WIDTH / 2f - button.getWidth() / 2f, Embergene.HEIGHT / 2f - button.getHeight() / 2f);
        sb.end();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void dispose() {
        button.dispose();
    }
}