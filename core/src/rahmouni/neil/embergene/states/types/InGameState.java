package rahmouni.neil.embergene.states.types;

import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.List;

import rahmouni.neil.embergene.Embergene;
import rahmouni.neil.embergene.IngameCamera;
import rahmouni.neil.embergene.champions.Champion;
import rahmouni.neil.embergene.champions.input.ChampionControllerManager;
import rahmouni.neil.embergene.champions.input.ChampionKeyboardManager;
import rahmouni.neil.embergene.champions.types.Mikhail;
import rahmouni.neil.embergene.hud.DamageBar;
import rahmouni.neil.embergene.network.Connection;
import rahmouni.neil.embergene.projectiles.Projectile;
import rahmouni.neil.embergene.states.State;
import rahmouni.neil.embergene.states.StateType;

public class InGameState extends State {
    public static List<Projectile> projectiles = new ArrayList<>();

    private Champion playerChampion;
    private List<Champion> champions = new ArrayList<>();
    private List<DamageBar> damageBars = new ArrayList<>();
    private Vector3 camPosition = new Vector3((int) (Embergene.WIDTH / 2f), (int) (Embergene.HEIGHT / 2f), 0);
    private long startTimestamp;

    private Viewport viewport;
    private Viewport hudViewport;
    private Texture background;
    private Texture t;

    InGameState() {
        super(StateType.IN_GAME);

        viewport = new FitViewport(Embergene.WIDTH, Embergene.HEIGHT, cam);
        cam.setToOrtho(false);
        cam.update();
        hudViewport = new FitViewport(Embergene.WIDTH, Embergene.HEIGHT, hudCam);
        hudCam.setToOrtho(false);
        hudCam.update();
        Stage stage = new Stage(viewport);
        new Stage(hudViewport);
        background = new Texture("images/background.png");

        addChampion(new Mikhail(850, 50), true);
        addChampion(new Mikhail(550, 50));

        ChampionControllerManager cm = new ChampionControllerManager(playerChampion);
        Controllers.addListener(cm);
        stage.addListener(new ChampionKeyboardManager(playerChampion, stage, cm));

        Pixmap pixmap = new Pixmap(20, 20, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fillRectangle(0, 0, 100, 100);
        t = new Texture(pixmap);
        pixmap.dispose();

        IngameCamera.setPosition(new Vector3((int) (Embergene.WIDTH / 2f), (int) (Embergene.HEIGHT / 2f), 0));

        startTimestamp = TimeUtils.millis();

        connection = new Connection();
    }

    @Override
    public void update(float dt) {
        //int camXmin = Embergene.WIDTH/2, camXmax = Embergene.WIDTH/2;
        //int camYmin = Embergene.HEIGHT/2, camYmax = Embergene.HEIGHT/2;
        int camXmin = (int) playerChampion.getSpriteCenterPosition().x, camXmax = (int) playerChampion.getSpriteCenterPosition().x;
        int camYmin = (int) playerChampion.getSpriteCenterPosition().y, camYmax = (int) playerChampion.getSpriteCenterPosition().y;
        for (Champion c : champions) {
            if (c.getSpriteCenterPosition().x > camXmax)
                camXmax = (int) c.getSpriteCenterPosition().x;
            if (c.getSpriteCenterPosition().x < camXmin)
                camXmin = (int) c.getSpriteCenterPosition().x;
            if (c.getSpriteCenterPosition().y > camYmax)
                camYmax = (int) c.getSpriteCenterPosition().y;
            if (c.getSpriteCenterPosition().y < camYmin)
                camYmin = (int) c.getSpriteCenterPosition().y;

            c.update(dt);
        }
        IngameCamera.update(dt,
                new Vector3((int) ((camXmin + camXmax) / 2f), (int) ((camYmin + camYmax) / 2f), 0),
                Math.max(Math.abs(camXmax), Math.abs(camXmin)),
                Math.max(Math.abs(camYmax), Math.abs(camYmin)));
        camPosition.set((int) ((camXmin + camXmax) / 2f), (int) ((camYmin + camYmax) / 2f), 0);
        cam.position.set(IngameCamera.getPosition());

        cam.viewportWidth = IngameCamera.getSize().x;
        cam.viewportHeight = IngameCamera.getSize().y;
        cam.update();

        for (DamageBar b : damageBars) b.update(dt);
        for (Projectile p : new ArrayList<>(projectiles)) p.update(dt);
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();

        sb.draw(background, 0, 0);
        sb.setColor(Color.GREEN);
        sb.draw(t, camPosition.x - 10, camPosition.y - 10);
        sb.setColor(Color.RED);
        sb.draw(t, IngameCamera.getPosition().x - 10, IngameCamera.getPosition().y - 10);
        sb.setColor(Color.BLUE);
        sb.draw(t, Embergene.WIDTH / 2f, Embergene.HEIGHT / 2f);
        sb.setColor(Color.WHITE);

        for (Projectile p : projectiles) p.render(sb);
        for (Champion c : champions) c.render(sb);

        sb.setProjectionMatrix(hudCam.combined);
        for (DamageBar b : damageBars) b.render(sb);

        sb.end();
    }

    @Override
    public void resize(int width, int height) {
        hudViewport.update(width, height);
        viewport.update(width, height);
    }

    @Override
    public void dispose() {
        for (Champion c : champions) c.dispose();
        for (DamageBar b : damageBars) b.dispose();
    }

    private void addChampion(Champion c, boolean isPlayer) {
        if (isPlayer) playerChampion = c;
        champions.add(c);
        damageBars.add(new DamageBar(c, champions.size() - 1));
    }

    private void addChampion(Champion c) {
        addChampion(c, false);
    }

    public Champion getPlayerChampion() {
        return playerChampion;
    }

    public long getStartTimestamp() {
        return startTimestamp;
    }
}
