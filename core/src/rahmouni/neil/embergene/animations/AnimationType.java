package rahmouni.neil.embergene.animations;

public enum AnimationType {
    MIKHAIL_PREVIEW("images/mikhail/preview.png", 4, 0.6f),
    MIKHAIL_STANDING_RIGHT("images/mikhail/standing_right.png", 4, 0.6f),
    MIKHAIL_LIGHT_ATTACK_RIGHT("images/mikhail/light_attack_right.png", 9, 0.3f);

    private String texturePath;
    private int frameCount;
    private float cycleTime;

    AnimationType(String texturePath, int frameCount, float cycleTime) {
        this.texturePath = texturePath;
        this.frameCount = frameCount;
        this.cycleTime = cycleTime;
    }

    public String getTexturePath() {
        return texturePath;
    }

    public int getFrameCount() {
        return frameCount;
    }

    public float getCycleTime() {
        return cycleTime;
    }
}
