package rahmouni.neil.embergene.animations;

import rahmouni.neil.embergene.champions.ChampionType;

public enum ChampionAnimationType {
    PREVIEW,
    STANDING_RIGHT,
    LIGHT_ATTACK_RIGHT;

    public AnimationType getAnimationType(ChampionType championType) {
        return AnimationType.valueOf(championType.name() + "_" + this.name());
    }
}