package rahmouni.neil.embergene.animations;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

public class Animation {

    private AnimationType type;
    private Array<TextureRegion> frames;
    private float maxFrameTime;
    private float currentFrameTime;
    private int frame;

    private Runnable onCycleEnd;

    public Animation(AnimationType type) {
        this.type = type;
        this.frames = new Array<>();

        TextureRegion region = new TextureRegion(new Texture(type.getTexturePath()));
        int frameWidth = region.getRegionWidth() / type.getFrameCount();
        for (int i = 0; i < type.getFrameCount(); i++) {
            frames.add(new TextureRegion(region, i * frameWidth, 0, frameWidth, region.getRegionHeight()));
        }

        this.maxFrameTime = type.getCycleTime() / type.getFrameCount();
        this.frame = 0;
    }

    public void update(float dt) {
        currentFrameTime += dt;
        while (currentFrameTime > maxFrameTime) {
            frame++;
            if (frame >= type.getFrameCount()) {
                frame = 0;
            }
            currentFrameTime -= maxFrameTime;
        }
    }

    public TextureRegion getFrame() {
        return frames.get(frame);
    }

    public float getDtUntilFrame(int frame) {
        return maxFrameTime * frame;
    }

    public float getDtUntilFinalFrame() {
        return maxFrameTime * type.getFrameCount();
    }

    public void dispose() {
        frames.get(0).getTexture().dispose();
    }
}