package rahmouni.neil.embergene;

import com.badlogic.gdx.Gdx;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rahmouni.neil.embergene.champions.ChampionType;

public class Player {
    private String connectionID;
    private String username;
    private ChampionType championType;

    public Player(String connectionID, String username) {
        this.connectionID = connectionID;
        this.username = username;
    }

    public static List<Player> getPlayerListFromJSONArray(JSONArray jsonArray) {
        ArrayList<Player> players = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                players.add(getPlayerFromJSONObject(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return players;
    }

    public static Player getPlayerFromJSONObject(JSONObject jsonObject) {
        try {
            return new Player(jsonObject.getString("_id"), jsonObject.getString("_id"));
        } catch (JSONException e) {
            e.printStackTrace();
            Gdx.app.log("RahNeil_N3:stackTrace", jsonObject.toString());
        }
        return null;
    }

    public String getConnectionID() {
        return connectionID;
    }

    public String getUsername() {
        return username;
    }

    public ChampionType getChampionType() {
        return championType;
    }

    public void setChampionType(ChampionType championType) {
        this.championType = championType;
    }
}
