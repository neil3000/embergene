package rahmouni.neil.embergene;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import rahmouni.neil.embergene.states.State;
import rahmouni.neil.embergene.states.types.TitleScreenState;

public class Embergene extends ApplicationAdapter {
    public static final int WIDTH = 1920;
    public static final int HEIGHT = 1080;

    private State state;
    private Runnable onStateChange;
    private SpriteBatch spriteBatch;

    @Override
    public void create() {
        spriteBatch = new SpriteBatch();

        Gdx.gl.glClearColor(0, 0, 0, 1);

        state = new TitleScreenState();
        state.setEmbergene(this);
        if (onStateChange != null) onStateChange.run();
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        state.update(Gdx.graphics.getDeltaTime());
        state.render(spriteBatch);
    }

    @Override
    public void resize(int width, int height) {
        state.resize(width, height);
    }

    @Override
    public void dispose() {
        state.dispose();
        spriteBatch.dispose();
    }

    public State getState() {
        return state;
    }

    public void setState(State newState) {
        state = newState;
        if (onStateChange != null) onStateChange.run();
    }

    public void setOnStateChange(Runnable newOnStateChange) {
        onStateChange = newOnStateChange;
    }
}
//TODO credit Zacchary Dempsey-Plante for the Pixellari font