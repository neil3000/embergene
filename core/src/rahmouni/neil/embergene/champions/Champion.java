package rahmouni.neil.embergene.champions;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import rahmouni.neil.embergene.animations.Animation;
import rahmouni.neil.embergene.animations.ChampionAnimationType;

public abstract class Champion {
    protected Vector2 position;
    protected boolean attacking = false;
    protected float attackCooldown = 0;
    protected Animation animation;
    private Vector2 velocity;
    private int walkingDir = 0;
    private boolean fastFalling = false;
    private int jumpLeft = 0;
    private Rectangle groundBounds;
    private ChampionType type;
    private float damage = 0;

    protected Champion(ChampionType type, int x, int y) {
        this.type = type;
        position = new Vector2(x, y);
        velocity = new Vector2(0, 0);
        setAnimation(ChampionAnimationType.STANDING_RIGHT);
    }


    //ATTACKS
    public abstract void lightAttackRight();


    //OTHERS FUCNTIONS
    protected void setAnimation(rahmouni.neil.embergene.animations.ChampionAnimationType ca) {
        if (this.animation != null) this.animation.dispose();
        this.animation = new Animation(ca.getAnimationType(type));
        this.groundBounds = new Rectangle(this.position.x, this.position.y, animation.getFrame().getRegionWidth(), animation.getFrame().getRegionHeight());
    }

    public void jump() {
        if (jumpLeft > 0) {
            velocity.y = 165;
            fastFalling = false;
            jumpLeft--;
        }
    }

    public int getWalkingDir() {
        return walkingDir;
    }

    public void setWalkingDir(int walkingDir) {
        this.walkingDir = walkingDir;
    }

    public boolean isFastFalling() {
        return fastFalling;
    }

    public void setFastFalling(boolean fastFalling) {
        this.fastFalling = fastFalling;
    }

    public void update(float dt) {
        //ATTACK
        attackCooldown -= dt;
        if (attackCooldown <= 0) {
            attackCooldown = 0;
            if (attacking) {
                setAnimation(ChampionAnimationType.STANDING_RIGHT);
                animation.update(Math.abs(attackCooldown));
                attacking = false;
            }
        }

        //GRAVITY
        position.add(300 * walkingDir * dt, -1000 * dt);
        if (fastFalling && velocity.y < 100) {
            position.add(0, -300 * dt);
        } else {
            fastFalling = false;
        }
        velocity.scl(10 * dt);

        //UPDATE POS
        position.add(velocity);
        if (position.y < 0) {
            position.y = 0;
            velocity.y = 0;
            jumpLeft = 3;
        }

        velocity.scl(.98f / (dt * 10));

        //UPDATE HITBOXES
        groundBounds.setPosition(position.x, position.y);

        //ANIM
        animation.update(dt);
    }

    private void inflictDamage(int damage) {
        this.damage += damage / 10000f;
    }

    public Rectangle getGroundBounds() {
        return groundBounds;
    }

    public void render(SpriteBatch sb) {
        sb.draw(this.animation.getFrame(), position.x - this.animation.getFrame().getRegionWidth() / 2f, position.y);
    }

    public ChampionType getType() {
        return type;
    }

    public float getDamage() {
        return damage;
    }

    public Vector2 getSpriteCenterPosition() {
        return new Vector2(position).add(0, animation.getFrame().getRegionHeight() / 2f);
    }

    public void dispose() {
        this.animation.dispose();
    }
}
