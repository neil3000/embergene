package rahmouni.neil.embergene.champions.input;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.mappings.Xbox;

import rahmouni.neil.embergene.champions.Champion;

public class ChampionControllerManager extends ControllerAdapter {

    private Champion champion;
    private boolean active = false;

    public ChampionControllerManager(Champion champion) {
        this.champion = champion;
    }

    @Override
    public boolean buttonDown(Controller controller, int buttonIndex) {
        active = true;
        if (buttonIndex == Xbox.B) {
            champion.jump();
            return true;
        } else if (buttonIndex == Xbox.A) {
            champion.lightAttackRight();
            return true;
        }
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisIndex, float value) {
        if (active) {
            if (axisIndex == Xbox.R_STICK_HORIZONTAL_AXIS) {
                champion.setWalkingDir(value > .5 ? 1 : value < -.5 ? -1 : 0);
                return true;
            } else if (axisIndex == Xbox.R_STICK_VERTICAL_AXIS) {
                champion.setFastFalling(value > .5);
                return true;
            }
        }
        return false;
    }

    @Override
    public void connected(Controller controller) {
        active = true;
    }

    @Override
    public void disconnected(Controller controller) {
        active = false;
    }

    void setInactive() {
        this.active = false;
    }
}
