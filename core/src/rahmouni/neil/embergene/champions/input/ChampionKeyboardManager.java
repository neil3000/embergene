package rahmouni.neil.embergene.champions.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;

import rahmouni.neil.embergene.champions.Champion;

public class ChampionKeyboardManager extends InputListener {

    private Champion champion;
    private ChampionControllerManager controllerManager;

    public ChampionKeyboardManager(Champion champion, Stage stage, ChampionControllerManager controllerManager) {
        InputMultiplexer multiplexer = new InputMultiplexer();
        Gdx.input.setInputProcessor(multiplexer);
        multiplexer.addProcessor(stage);

        this.champion = champion;
        this.controllerManager = controllerManager;
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        controllerManager.setInactive();

        return false;
    }

    @Override
    public boolean keyDown(InputEvent event, int keycode) {
        controllerManager.setInactive();

        if (keycode == Input.Keys.SPACE ||
                keycode == Input.Keys.UP ||
                keycode == Input.Keys.Z ||
                keycode == Input.Buttons.FORWARD) {

            champion.jump();
            return true;
        }
        if (keycode == Input.Keys.SHIFT_LEFT ||
                keycode == Input.Keys.DOWN ||
                keycode == Input.Keys.S) {

            champion.setFastFalling(true);
            return true;
        }
        if (keycode == Input.Keys.RIGHT ||
                keycode == Input.Keys.D ||
                keycode == Input.Buttons.RIGHT) {

            champion.setWalkingDir(1);
            return true;
        }
        if (keycode == Input.Keys.LEFT ||
                keycode == Input.Keys.Q ||
                keycode == Input.Buttons.LEFT) {

            champion.setWalkingDir(-1);
            return true;
        }
        if (keycode == Input.Keys.C) {
            champion.lightAttackRight();
            return true;
        }
        return false;
    }

    @Override
    public boolean keyUp(InputEvent event, int keycode) {
        if (champion.isFastFalling()) {
            if (keycode == Input.Keys.SHIFT_LEFT ||
                    keycode == Input.Keys.DOWN ||
                    keycode == Input.Keys.S) {

                champion.setFastFalling(false);
                return true;
            }
        }
        if (champion.getWalkingDir() == 1) {
            if (keycode == Input.Keys.RIGHT ||
                    keycode == Input.Keys.D ||
                    keycode == Input.Buttons.RIGHT) {

                champion.setWalkingDir(0);
                return true;
            }
        } else if (champion.getWalkingDir() == -1) {
            if (keycode == Input.Keys.LEFT ||
                    keycode == Input.Keys.Q ||
                    keycode == Input.Buttons.LEFT) {

                champion.setWalkingDir(0);
                return true;
            }
        }
        return false;
    }
}
