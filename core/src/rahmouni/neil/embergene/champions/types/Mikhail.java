package rahmouni.neil.embergene.champions.types;

import com.badlogic.gdx.math.Vector2;

import rahmouni.neil.embergene.animations.ChampionAnimationType;
import rahmouni.neil.embergene.champions.Champion;
import rahmouni.neil.embergene.champions.ChampionType;
import rahmouni.neil.embergene.projectiles.Projectile;
import rahmouni.neil.embergene.projectiles.ProjectileType;

public class Mikhail extends Champion {
    private float lightAttackRight_projectileCooldown = -1;

    public Mikhail(int x, int y) {
        super(ChampionType.MIKHAIL, x, y);
    }

    @Override
    public void lightAttackRight() {
        if (attackCooldown == 0) {
            setAnimation(ChampionAnimationType.LIGHT_ATTACK_RIGHT);
            attacking = true;
            attackCooldown = animation.getDtUntilFinalFrame();
            lightAttackRight_projectileCooldown = animation.getDtUntilFrame(4);
        }
    }

    @Override
    public void update(float dt) {
        super.update(dt);

        if (lightAttackRight_projectileCooldown != -1) {
            lightAttackRight_projectileCooldown -= dt;

            if (lightAttackRight_projectileCooldown <= 0) {
                lightAttackRight_projectileCooldown = -1;
                new Projectile(ProjectileType.BULLET,
                        new Vector2(position).add(32, 46),
                        new Vector2(1, 0));
            }
        }
    }
}