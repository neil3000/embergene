package rahmouni.neil.embergene.champions;

public enum ChampionType {
    MIKHAIL("Mikhail", "champion_mikhail"),
    SCYRGAL("Scyrgal", null);

    private String displayName;
    private String discordImage;

    ChampionType(String displayName, String discordImage) {
        this.displayName = displayName;
        this.discordImage = discordImage;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDiscordImage() {
        return discordImage;
    }
}
