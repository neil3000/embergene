package rahmouni.neil.embergene.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import rahmouni.neil.embergene.Embergene;
import rahmouni.neil.embergene.Utils;
import rahmouni.neil.embergene.animations.Animation;
import rahmouni.neil.embergene.animations.ChampionAnimationType;
import rahmouni.neil.embergene.champions.Champion;
import rahmouni.neil.embergene.champions.ChampionType;

public class DamageBar {
    private static Texture damageBar;

    private Champion champion;
    private ChampionType championType;
    private float damage;
    private String username;
    private int pos;
    private Animation championPreview;
    private BitmapFont pixellari32;
    private Color bgColor = new Color(0, 0, 0, 1);

    public DamageBar(ChampionType championType, float damage, int pos) {
        this.championType = championType;
        this.damage = damage;
        this.pos = pos;

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/pixellari.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 32;
        parameter.borderWidth = 2;
        parameter.borderColor = Color.BLACK;
        pixellari32 = generator.generateFont(parameter);
        generator.dispose();

        damageBar = new Texture("images/damage_bar.png");
        championPreview = new Animation(ChampionAnimationType.PREVIEW.getAnimationType(championType));
    }

    public DamageBar(Champion champion, int pos) {
        DamageBar b = new DamageBar(champion.getType(), champion.getDamage(), pos);
        b.champion = champion;
    }

    public void update(float dt) {
        bgColor = new Color((float) Utils.map(this.damage, .67, 1, 1, 0),
                (float) Utils.map(this.damage, .33, .67, 1, 0),
                (float) Utils.map(this.damage, 0, .33, 1, 0), 1);
        championPreview.update(dt);
    }

    public void render(SpriteBatch sb) {
        sb.draw(championPreview.getFrame(),
                Embergene.WIDTH - championPreview.getFrame().getRegionWidth(),
                Embergene.HEIGHT - 144 * pos - 112);

        sb.setColor(bgColor);
        sb.draw(damageBar, Embergene.WIDTH - damageBar.getWidth(), Embergene.HEIGHT - 144 * pos - damageBar.getHeight() - 112);
        sb.setColor(Color.WHITE);

        pixellari32.setColor(Color.WHITE);
        pixellari32.draw(sb, username == null ? this.championType.getDisplayName() : username,
                Embergene.WIDTH - damageBar.getWidth() + 64,
                Embergene.HEIGHT - 144 * pos - 120);
    }

    public void dispose() {
        damageBar.dispose();
        championPreview.dispose();
        pixellari32.dispose();
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
