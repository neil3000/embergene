package rahmouni.neil.embergene.network.events;

import java.util.List;

import rahmouni.neil.embergene.Player;

public interface JoinedRoomEvent {
    void call(String roomID, List<Player> players);
}
