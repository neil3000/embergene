package rahmouni.neil.embergene.network.events;

public interface ConnectionEstablishedEvent {
    void call();
}
