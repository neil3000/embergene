package rahmouni.neil.embergene.network.events;

public interface ConnectionLostEvent {
    void call();
}
