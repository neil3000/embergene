package rahmouni.neil.embergene.network.events;

import rahmouni.neil.embergene.Player;

public interface PlayerLeftRoomEvent {
    void call(Player player);
}
