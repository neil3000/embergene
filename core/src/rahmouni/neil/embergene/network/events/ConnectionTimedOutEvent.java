package rahmouni.neil.embergene.network.events;

public interface ConnectionTimedOutEvent {
    void call();
}
