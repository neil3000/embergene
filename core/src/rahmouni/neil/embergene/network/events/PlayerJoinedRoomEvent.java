package rahmouni.neil.embergene.network.events;

import rahmouni.neil.embergene.Player;

public interface PlayerJoinedRoomEvent {
    void call(Player player);
}
