package rahmouni.neil.embergene.network;

import rahmouni.neil.embergene.Player;
import rahmouni.neil.embergene.network.events.PlayerJoinedRoomEvent;

public abstract class NetworkEvent implements PlayerJoinedRoomEvent {
    public abstract void call(Object... args);

    @Override
    public void call(Player player) {

    }
}
