package rahmouni.neil.embergene.network;

import com.badlogic.gdx.Gdx;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import rahmouni.neil.embergene.Player;
import rahmouni.neil.embergene.network.events.ConnectionErrorEvent;
import rahmouni.neil.embergene.network.events.ConnectionEstablishedEvent;
import rahmouni.neil.embergene.network.events.ConnectionLostEvent;
import rahmouni.neil.embergene.network.events.ConnectionTimedOutEvent;
import rahmouni.neil.embergene.network.events.JoinedRoomEvent;
import rahmouni.neil.embergene.network.events.PlayerJoinedRoomEvent;
import rahmouni.neil.embergene.network.events.PlayerLeftRoomEvent;

public class Connection {

    private Socket socket;

    public Connection() {
        try {
            socket = IO.socket("http://localhost:8080");
            socket.connect();
        } catch (Exception e) {
            Gdx.app.log("RahNeil_N3:stackTrace", "could not connect to server");
            e.printStackTrace();
        }
    }

    public void sendRoomSearchingPacket() {
        socket.emit("RahNeil_N3:roomSearching");
    }

    public void addListener(final ConnectionEstablishedEvent event) {
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        Gdx.app.log("RahNeil_N3:NetworkEvent", "ConnectionEstablishedEvent called");
                        event.call();
                    }
                });
            }
        });
    }

    public void addListener(final ConnectionErrorEvent event) {
        socket.on(Socket.EVENT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        Gdx.app.log("RahNeil_N3:NetworkEvent", "ConnectionErrorEvent called");
                        event.call();
                    }
                });
            }
        });
    }

    public void addListener(final ConnectionTimedOutEvent event) {
        socket.on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        Gdx.app.log("RahNeil_N3:NetworkEvent", "ConnectionTimedOutEvent called");
                        event.call();
                    }
                });
            }
        });
    }

    public void addListener(final ConnectionLostEvent event) {
        socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        Gdx.app.log("RahNeil_N3:NetworkEvent", "ConnectionLostEvent called");
                        event.call();
                    }
                });
            }
        });
    }

    public void addListener(final JoinedRoomEvent event) {
        socket.on("RahNeil_N3:joinedRoom", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        Gdx.app.log("RahNeil_N3:NetworkEvent", "JoinedRoomEvent called");

                        JSONObject data = (JSONObject) args[0];
                        try {
                            event.call(data.getString("roomID"), Player.getPlayerListFromJSONArray(data.getJSONArray("players")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    public void addListener(final PlayerJoinedRoomEvent event) {
        socket.on("RahNeil_N3:playerJoinedRoom", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        Gdx.app.log("RahNeil_N3:NetworkEvent", "PlayerJoinedRoomEvent called");

                        JSONObject data = (JSONObject) args[0];
                        try {
                            event.call(Player.getPlayerFromJSONObject(data.getJSONObject("player")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    public void addListener(final PlayerLeftRoomEvent event) {
        socket.on("RahNeil_N3:playerLeftRoom", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        Gdx.app.log("RahNeil_N3:NetworkEvent", "PlayerLeftRoomEvent called");

                        JSONObject data = (JSONObject) args[0];
                        try {
                            event.call(Player.getPlayerFromJSONObject(data.getJSONObject("player")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}
