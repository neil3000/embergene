var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var Room = require('./room.js');

var rooms = [];
var roomsWaiting = [];
var roomInt = 999;

server.listen(8080, function() {
    console.log("Server is now running...");
});

io.on('connection', function(socket) {
    console.log("Player connected!");

    socket.on('RahNeil_N3:roomSearching', function() {
        console.log("player searched room");

        if (roomsWaiting.length==0) {
            roomInt++;
            if (roomInt==10000) roomInt = 1000;

            var room = new Room(io, '#'+roomInt);
            rooms.push(room);
            roomsWaiting.push(room);
            console.log("created new room #"+roomInt);
        }
        roomsWaiting[0].addPlayer(socket);
     });

    socket.on('disconnect', function() {
        console.log("Player disconnected!");

        for (var i = 0; i < rooms.length; i++) {
            rooms[i].removePlayer(socket);
        }
    });
});