var Player = require('./player.js');

class Room {
    constructor(io, id) {
        this._io = io;
        this._id = id;
        this._players = [];
    }

    addPlayer(socket) {
        var socketPlayer = new Player(socket.id);
        this._players.push(socketPlayer);
        socket.join(this._id);

        socket.emit("RahNeil_N3:joinedRoom", {roomID: this._id, players: this._players});
        this.emitExceptSocket(socket, "RahNeil_N3:playerJoinedRoom", {player: socketPlayer});
    }

    removePlayer(socket) {
        for (var i = 0; i < this._players.length; i++) {
            if (this._players[i].id === socket.id) {
                this.emit("RahNeil_N3:playerLeftRoom", socket.id);
                this._players.pop(this._players[i]);
            }
        }
    }

    emit(event, data) {
        this._io.in(this._id).emit(event, data);
        console.log("["+this._id+"] "+event+" | "+data);
    }

    emitExceptSocket(socket, event, data) {
        socket.broadcast.to(this._id).emit(event, data);
        console.log("["+this._id+"] "+event+" | "+data);
    }
}

module.exports = Room;