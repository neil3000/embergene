package rahmouni.neil.embergene.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import net.arikia.dev.drpc.DiscordEventHandlers;
import net.arikia.dev.drpc.DiscordRPC;
import net.arikia.dev.drpc.DiscordRichPresence;

import rahmouni.neil.embergene.Embergene;
import rahmouni.neil.embergene.states.StateType;
import rahmouni.neil.embergene.states.types.InGameState;
import rahmouni.neil.embergene.states.types.InLobbyState;

public class DesktopLauncher {

    public static void main(String[] arg) {
        final Embergene embergene = new Embergene();

        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = Embergene.WIDTH / 2;
        config.height = Embergene.HEIGHT / 2;
        config.title = "Embergene";
        new LwjglApplication(embergene, config);

        DiscordEventHandlers.Builder handlers = new DiscordEventHandlers.Builder();
        DiscordRPC.discordInitialize("701849182419550309", handlers.build(), true);

        embergene.setOnStateChange(new Runnable() {
            @Override
            public void run() {
                DiscordRichPresence.Builder rich = new DiscordRichPresence.Builder(embergene.getState().getType().getDisplayName());

                if (embergene.getState().getType() == StateType.IN_GAME) {
                    InGameState s = (InGameState) embergene.getState();
                    rich.setDetails("Ranked 1v1");
                    rich.setBigImage(s.getPlayerChampion().getType().getDiscordImage(), s.getPlayerChampion().getType().getDisplayName());
                    rich.setStartTimestamps(s.getStartTimestamp());
                } else if (embergene.getState().getType() == StateType.IN_LOBBY) {
                    InLobbyState s = (InLobbyState) embergene.getState();
                    rich.setDetails("Ranked 1v1");
                    rich.setStartTimestamps(s.getStartTimestamp());
                }

                DiscordRPC.discordUpdatePresence(rich.build());
                DiscordRPC.discordRegister("701849182419550309", "");
            }
        });
    }
}